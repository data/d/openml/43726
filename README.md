# OpenML dataset: Primary-breast-cancer-vs-Normal-breast-tissue

https://www.openml.org/d/43726

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Tumor microRNA expression profiling identifies circulating microRNAs for earlier breast cancer detection.
Due to microRNA role in tumorigenesis and remarkable stability in body fluids, microRNAs (miRNAs) are emerging as a promising diagnostic tool. The aim of this study was to identify tumor miRNA signatures for the discrimination of breast cancer and the intrinsic molecular subtypes, and the study in plasma of the status of the most significant ones in order to identify potential circulating biomarkers for breast cancer detection.
https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE58606
Content
The cols are each microRNA name and the rows are the samples. The values are microarray expression data.
Acknowledgements
Matamala N, Vargas MT, Gonzlez-Cmpora R, Miambres R et al. Tumor microRNA expression profiling identifies circulating microRNAs for early breast cancer detection. Clin Chem 2015 Aug;61(8):1098-106. PMID: 26056355
Inspiration
Use this data to practice making predictive models from machine learning/deep learning algorithms on gene expression profiling data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43726) of an [OpenML dataset](https://www.openml.org/d/43726). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43726/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43726/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43726/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

